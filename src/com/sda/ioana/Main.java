package com.sda.ioana;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //        printName();
        printTableWithStringFormat();
        printTable();
        printNumber();
        triangle(10);
        printNumberTriangle(10);
        printNumberTriangle(readNumberFromConsole());

    }

    public static void printName() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Introdu numele de familie:");
        String name = scan.nextLine();
        System.out.println("Introdu prenumele:");
        String surname = scan.nextLine();

        System.out.printf("Numele de familie este %10s si prenumele este %-10s", name, surname);
    }

    public static void printTable() {
        System.out.printf("%-20s %s\n", "Exam name", "Exam Grade");
        System.out.println("--------------------------------");
        System.out.printf("%-20s %s\n", "Java", "A");
        System.out.printf("%-20s %s\n", "PHP", "B");
        System.out.printf("%-20s %s\n", "VB NET", "A");
        System.out.println("--------------------------------");
    }

    private static void printTableWithStringFormat() {
        System.out.println(String.format("%-20s %s", "Exam Name", "Exam Grade"));
        System.out.println("-------------------------------------------");
        System.out.println(String.format("%-24s %s", "Java", "A"));
        System.out.println(String.format("%-24s %s", "Php", "B"));
        System.out.println(String.format("%-24s %s", "VB net", "A"));
        System.out.println("-------------------------------------------");
    }

    public static void printNumber() {
        System.out.printf("Numarul %.2f este formatat\n", 5.5);
        System.out.printf("Numarul %.1f este formatat\n", 7.3);
    }

    public static int triangle (int n){

        for (int i = 1; i <= n; i++){
            for (int j = 1; j <= i; j++){
                System.out.print(j + " ");
            }
            System.out.println();
        }
        return 0;
    }

    public static void printNumberTriangle(int number){
        String text = "";
        for (int i = 1; i <= number; i++){
            text = text + i + " ";
            System.out.println(text);
        }
    }

    public static int readNumberFromConsole(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu un numar");
        return scanner.nextInt();
    }



}
