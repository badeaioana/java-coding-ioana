package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task15 {
    public static void main(String[] args) {
        Task15 task = new Task15();
        task.sneeze();
    }

    public void sneeze() {
        System.out.println("Enter a text");
        String text = ScannerUtils.getScanner().nextLine();
        Pattern pattern = Pattern.compile("acho+!");
        Matcher matcher = pattern.matcher(text);
        System.out.println("Userul a stranutat: " + matcher.matches());
    }
}


