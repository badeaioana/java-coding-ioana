package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Task14 {
    public static void main(String[] args) {
        Task14 task = new Task14();
        task.howManyDaysLeft();
    }

    public void howManyDaysLeft(){
        System.out.println("Introduce the date under the format: dd/MM/yyyy");
        String input = ScannerUtils.getScanner().nextLine();
        LocalDate inputDate = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        LocalDate currentDate = LocalDate.now();
        Period daysTillInputDate = Period.between(currentDate, inputDate);
        System.out.println("There are " + currentDate.until(inputDate, ChronoUnit.DAYS) + " days till " + inputDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
    }
}
