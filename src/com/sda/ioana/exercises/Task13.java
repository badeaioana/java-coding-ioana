package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task13 {
    public static void main(String[] args) {
        Task13 task = new Task13();
        System.out.println("Cel mai lung si de numere consecutive este " + task.longestConsecutiveNumbers());
    }

    public int longestConsecutiveNumbers() {
        System.out.println("Introduce 10 numbers");
        int[] numbersFromUser = new int[10];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getNumberFromInput();
        }
        int consecutiveNumbers = 0;
        int longestNumbers = 0;
        for (int i = 0; i < numbersFromUser.length; i++) {
            if (numbersFromUser[i] < numbersFromUser[i + 1]) {
                consecutiveNumbers++;
                if (longestNumbers < consecutiveNumbers) {
                    longestNumbers = consecutiveNumbers;
                }
            } else {
                consecutiveNumbers = 1;
            }
            if (i == numbersFromUser.length - 2) {
                break;
            }
        }
        return longestNumbers;
    }
}
