package com.sda.ioana.exercises;
import java.util.Scanner;
public class HangmanTask {

        private static String[] words = {"terminator", "banana", "computer", "cow", "rain", "water"};
        private static String word = words[(int) (Math.random() * words.length)];
        private static String asterisk = new String(new char[word.length()]).replace("\0", "*");
        private static int count = 0;

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            while (count < 7 && asterisk.contains("*")) {
                System.out.println("Guess any letter in the word");
                System.out.println(asterisk);
                String guess = scanner.next();
                hang(guess);
            }
            scanner.close();
        }

        public static void hang(String guess) {
            String newChar = "";
            for (int i = 0; i < word.length(); i++) {
                if (word.charAt(i) == guess.charAt(0)) {
                    newChar += guess.charAt(0);
                } else if (asterisk.charAt(i) != '*') {
                    newChar += word.charAt(i);
                } else {
                    newChar += "*";
                }
            }
            if (asterisk.equals(newChar)) {
                count++;
                hangmanImage();
            } else {
                asterisk = newChar;
            }
            if (asterisk.equals(word)) {
                System.out.println("Correct! You win! The word was " + word);
            }
        }

        public static void hangmanImage() {
            if (count == 1) {
                System.out.println("Wrong guess, try again");
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println();
                System.out.println("___|___");
                System.out.println();
            }
            if (count == 2) {
                System.out.println("Wrong guess, try again");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            if (count == 3) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   | ");
                System.out.println("___|___");
            }
            if (count == 4) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            if (count == 5) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |           |");
                System.out.println("   |           |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            if (count == 6) {
                System.out.println("Wrong guess, try again");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |           |");
                System.out.println("   |           |");
                System.out.println("   |          / \\ ");
                System.out.println("___|___      /   \\");
            }
            if (count == 7) {
                System.out.println("GAME OVER!");
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |          _|_");
                System.out.println("   |         / | \\");
                System.out.println("   |          / \\ ");
                System.out.println("___|___      /   \\");
                System.out.println("GAME OVER! The word was " + word);
            }
        }
    }


