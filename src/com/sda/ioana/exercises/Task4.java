package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task4 {

    public static void main(String[] args) {
        Task4 task = new Task4();
        task.printPrimeNumbers();
    }

    public void printPrimeNumbers() {
        System.out.println("Enter a positive number");
        int number = ScannerUtils.getNumberFromInput();
        int count;

        for (int j = 2; j < number; j++) {
            count = 0;
            for (int i = 1; i <= j; i++) {
                if (j % i == 0) {
                    count++;
                }
            }
            if (count == 2)
                System.out.println(j);
        }
    }
}

