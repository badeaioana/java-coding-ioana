package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task7 {
    public static void main(String[] args) {
        Task7 task = new Task7();
        task.drawWaves(4, 4);
    }

    public void drawWaves(int lines, int wavesNumber) {
        for (int i = 1; i <= lines; i++) {
            for (int j = 1; j <= wavesNumber; j++) {
                int numberOfSpacesBetween = (lines * 2) - (i * 2);
                String star = "*";
                String space = "";
                String line = "%" + i + "s" + "%" + numberOfSpacesBetween + "s" + "%-" + i + "s";
                if (numberOfSpacesBetween == 0) {
                    line = "%" + i + "s" + "%s" + "%-" + i + "s";
                }
                System.out.printf(line, star, space, star);
            }
            System.out.println();
        }

    }
}
