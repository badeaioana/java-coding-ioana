package com.sda.ioana.exercises;
import com.sda.ioana.utils.ScannerUtils;

import java.util.Scanner;

public class Task6 {

    public static void main(String[] args) {

    }

    public void resultOfOperation(){
        float result = 0;
        Scanner scanner = ScannerUtils.getScanner();
        System.out.println("Insert first number");
        float firstNumber = ScannerUtils.getFloatFromConsole();
        System.out.println("Insert mathematical operation");
        String symbol = ScannerUtils.getScanner().nextLine();
        System.out.println("Insert second number");
        float secondNumber = ScannerUtils.getFloatFromConsole();

        switch (symbol){
            case "/":
                result = firstNumber / secondNumber;
                System.out.println("Result for the calculation is " + result);
                break;
            case "*":
                result = firstNumber * secondNumber;
                System.out.println("Result for the calculation is " + result);
                break;
            case "+":
                result = firstNumber + secondNumber;
                System.out.println("Result for the calculation is " + result);
                break;
            case "-":
                result = firstNumber - secondNumber;
                System.out.println("Result for the calculation is " + result);
                break;
            default:
                System.out.println("Invalid symbol");
                System.out.println("Cannot calculate");
        }

    }

}
