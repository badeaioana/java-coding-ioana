package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task12 {
    public static void main(String[] args) {
        Task12 task = new Task12();
        task.printNumbersThatOccurredTwice();
    }

    public void printNumbersThatOccurredTwice() {
        System.out.println("Introduce 10 arbitrarily large numbers");
        int[] numbersFromUser = new int[10];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getNumberFromInput();
        }
        for (int i = 0; i < numbersFromUser.length; i++) {
            if (numbersFromUser[i] == -1) {
                continue;
            }
            int duplicateNumber = -1;
            for (int j = i + 1; j < numbersFromUser.length; j++) {
                if (numbersFromUser[i] == numbersFromUser[j]) {
                    duplicateNumber = numbersFromUser[i];
                    numbersFromUser[j] = -1;
                }
            }
            if (duplicateNumber != -1) {
                System.out.println(duplicateNumber);
            }
        }
    }
}

