package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task10 {
    public static void main(String[] args) {
        Task10 task = new Task10();
        task.doubleTheWordsInAText();
    }

    public void doubleTheWordsInAText() {
        System.out.println("Introduce a text");
        String text = ScannerUtils.getScanner().nextLine();
        String[] arrayOfText = text.split(" ");
        for (String element : arrayOfText) {
            System.out.print(element + " " + element + " ");
        }
    }
}
