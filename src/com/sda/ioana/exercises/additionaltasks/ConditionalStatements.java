package com.sda.ioana.exercises.additionaltasks;

import com.sda.ioana.utils.ScannerUtils;

public class ConditionalStatements {
    public static void main(String[] args) {
        ConditionalStatements task = new ConditionalStatements();
//        task.greatestNumberFromUser();
//        task.floatingNumberFromUser();
//        task.daysOfWeekBasedOnInputNumber();
//        task.testIfTheNumbersAreTheSame();
//        task.numberOfDaysInAMonth();
//        task.printVowelOrConsonant();
//        task.leapYear();
        task.theFirst10NaturalNumbers();
//        task.checkInteger();
        task.increasingOrDecreasingNumbers();
    }

    public void greatestNumberFromUser(){
        System.out.println("Enter 3 numbers");
        int[] numbersFromUser = new int[3];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getNumberFromInput();
        }

        System.out.println("First number: " + numbersFromUser[0]);
        System.out.println("Second number: " + numbersFromUser[1]);
        System.out.println("Third number: " + numbersFromUser[2]);

        if (numbersFromUser[0] > numbersFromUser[1] && numbersFromUser[0] > numbersFromUser[2]){
            System.out.println("The greatest number is: " + numbersFromUser[0]);
        }else if (numbersFromUser[1] > numbersFromUser[0] && numbersFromUser[1] > numbersFromUser[2]){
            System.out.println("The greatest number is: " + numbersFromUser[1]);
        }else {
            System.out.println("The greatest number is: " + numbersFromUser[2]);
        }
    }

    public void floatingNumberFromUser(){
        System.out.println("Enter any number");
        double input = ScannerUtils.getDoubleFromInput();

        if (input == 0){
            System.out.println("Zero");
        }else if (input < 0){
            System.out.println("Negative");
        }else if (input > 0){
            System.out.println("Positive");
        }
        if (input < 1){
            System.out.println("Small");
        }
        if (input > 1000000){
            System.out.println("Large");
        }
    }

    public void daysOfWeekBasedOnInputNumber(){
        System.out.println("Enter a number to get the day of the week");
        int input = ScannerUtils.getNumberFromInput();

        switch (input){
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                System.out.println("Not a day of the week");
        }
    }

    public void testIfTheNumbersAreTheSame(){
        System.out.println("Enter two numbers with 3 decimals");
        double[] input = new double[2];
        for (int i = 0; i < input.length; i++){
            input[i] = ScannerUtils.getDoubleFromInput();
        }
        input[0] = Math.round(input[0] * 1000);
        input[1] = Math.round(input[1] * 1000);


        if (input[0] == input[1]){
            System.out.println("They are the same");
        }else{
            System.out.println("They are different");
        }
    }

    public void numberOfDaysInAMonth(){
        System.out.println("Input a month number");
        int month = ScannerUtils.getNumberFromInput();

        System.out.println("Input a year");
        int year = ScannerUtils.getNumberFromInput();

        String monthName = "";
        int numberOfDaysInAMonth = 0;

        switch (month){
            case 1:
                monthName = "January";
                numberOfDaysInAMonth = 31;
                break;
            case 2:
                monthName = "February";
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))){
                    numberOfDaysInAMonth = 29;
                }else{
                    numberOfDaysInAMonth = 28;
                }
                break;
            case 3:
                monthName = "March";
                numberOfDaysInAMonth = 31;
                break;
            case 4:
                monthName = "April";
                numberOfDaysInAMonth = 30;
                break;
            case 5:
                monthName = "May";
                numberOfDaysInAMonth = 31;
                break;
            case 6:
                monthName = "June";
                numberOfDaysInAMonth = 30;
                break;
            case 7:
                monthName = "July";
                numberOfDaysInAMonth = 31;
                break;
            case 8:
                monthName = "August";
                numberOfDaysInAMonth = 31;
                break;
            case 9:
                monthName = "September";
                numberOfDaysInAMonth = 30;
                break;
            case 10:
                monthName = "October";
                numberOfDaysInAMonth = 31;
                break;
            case 11:
                monthName = "November";
                numberOfDaysInAMonth = 30;
                break;
            case 12:
                monthName = "December";
                numberOfDaysInAMonth = 31;
                break;
        }
        System.out.println(monthName + " " + year + " has " + numberOfDaysInAMonth + " days \n");
    }

    public void printVowelOrConsonant(){
        System.out.println("Enter a letter of the alphabet");
        String input = ScannerUtils.getScanner().nextLine();

        boolean uppercase = input.charAt(0) >= 65 && input.charAt(0) <= 90;
        boolean lowercase = input.charAt(0) >= 97 && input.charAt(0) <= 122;
        boolean vowels = input.equals("a") || input.equals("e") || input.equals("i") || input.equals("o") || input.equals("u");

        if (input.length() > 1){
            System.out.println("Error. Not a single character.");
        }else if (!(uppercase || lowercase)){
            System.out.println("Error. Not a letter.");
        }else if (vowels){
            System.out.println("The letter is a vowel");
        }else {
            System.out.println("The letter is a consonant");
        }
    }

    public void leapYear(){
        System.out.println("Enter a year");
        int year = ScannerUtils.getNumberFromInput();

        boolean x = (year % 4) == 0;
        boolean y = (year % 100) != 0;
        boolean z = ((year % 100 == 0) && (year % 400 == 0));

        if (x && (y || z)){
            System.out.println(year + " is a leap year");
        }else{
            System.out.println(year + " is not a leap year");
        }
    }

    public void theFirst10NaturalNumbers(){
        System.out.println("The first 10 natural numbers are: ");
        for (int i = 1; i <= 10; i++){
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public void checkInteger(){
        System.out.println("Enter a number");
        int input = ScannerUtils.getNumberFromInput();

        if (input == 0){
            System.out.println("The number is 0");
        }else if(input < 0){
            System.out.println("The number is negative");
        }else{
            System.out.println("The number is positive");
        }
    }

    public void increasingOrDecreasingNumbers(){
        System.out.println("Enter 3 numbers");
        int[] input = new int[3];

        for (int i = 0; i < input.length; i++){
            input[i] = ScannerUtils.getNumberFromInput();
        }
        if (input[0] < input[1] && input[1] < input[2]){
            System.out.println("The numbers are increasing");
        }else if (input[0] > input[1] && input[1] > input[2]){
            System.out.println("The numbers are decreasing");
        }else{
            System.out.println("Neither decreasing or increasing");
        }
    }
}
