package com.sda.ioana.exercises.additionaltasks;

import com.sda.ioana.utils.ScannerUtils;

import java.util.Arrays;

public class Basic {
    public static void main(String[] args) {
        Basic basic = new Basic();
        basic.valueOfExpression();
//        basic.inputEquality();
//        basic.checkIfNumbersAreBetweenAValue();
        basic.twoDimensionalArray();

        int[][] twodm = {
                {10, 20, 30},
                {40, 50, 60}
        };
        System.out.print("Original Array:\n");
        print_array(twodm);
        System.out.println("After changing the rows and columns of the transpose array:");
        transpose(twodm);
    }



    public void valueOfExpression() {
        int number = (101 + 0) / 3;
        double number2 = 3.0e-6 * 10000000.1;
        boolean value = true && true;
        boolean value2 = false && true;
        boolean value3 = (false && false) || (true && true);
        boolean value4 = (false || false) && (true && true);

        System.out.println("(101 + 0) / 3 -> " + number);
        System.out.println("3.0e-6 * 10000000.1 -> " + number2);
        System.out.println("true && true -> " + value);
        System.out.println("false && true -> " + value2);
        System.out.println("(false && false) || (true && true) -> " + value3);
        System.out.println("(false || false) && (true && true) -> " + value4);
    }

    public void inputEquality() {
        System.out.println("Enter 4 numbers");
        int[] numbersFromUser = new int[4];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getNumberFromInput();
        }
        System.out.println("Input first number " + numbersFromUser[0]);
        System.out.println("Input second number " + numbersFromUser[1]);
        System.out.println("Input third number " + numbersFromUser[2]);
        System.out.println("Input fourth number " + numbersFromUser[3]);

        if (numbersFromUser[0] == numbersFromUser[1] && numbersFromUser[2] == numbersFromUser[3] && numbersFromUser[3] == numbersFromUser[0]) {
            System.out.println("Numbers are equal");
        } else {
            System.out.println("Numbers are not equal");
        }
    }

    public void checkIfNumbersAreBetweenAValue() {
        System.out.println("Enter 2 numbers");
        double[] numbersFromUser = new double[2];
        for (int i = 0; i < numbersFromUser.length; i++) {
            numbersFromUser[i] = ScannerUtils.getDoubleFromInput();
        }
        System.out.println(numbersFromUser[0] > 0 && numbersFromUser[0] < 1 && numbersFromUser[1] > 0 && numbersFromUser[1] < 1);
    }

    public void twoDimensionalArray() {

        boolean[][] array = new boolean[][]{{true, false, true}, {false, true, false}};
        int rowsLength = array.length;
        int columnsLength = array[0].length;

        for (int i = 0; i < rowsLength; i++) {
            for (int j = 0; j < columnsLength; j++) {
                if (array[i][j]) {
                    System.out.print(" t ");
                } else {
                    System.out.print(" f ");
                }
            }
            System.out.println();
        }
    }

    private static void transpose(int[][] twodm) {
        int[][] newtwodm = new int[twodm[0].length][twodm.length];

        for (int i = 0; i < twodm.length; i++) {
            for (int j = 0; j < twodm[0].length; j++) {
                newtwodm[j][i] = twodm[i][j];
            }
        }

        print_array(newtwodm);
    }

    private static void print_array(int[][] twodm) {
        for (int i = 0; i < twodm.length; i++) {
            for (int j = 0; j < twodm[0].length; j++) {
                System.out.print(twodm[i][j] + " ");
            }
            System.out.println();
        }
    }
}
