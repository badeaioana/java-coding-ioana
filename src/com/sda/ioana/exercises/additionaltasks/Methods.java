package com.sda.ioana.exercises.additionaltasks;

import com.sda.ioana.utils.ScannerUtils;

import java.util.Arrays;

public class Methods {
    public static void main(String[] args) {
        Methods task = new Methods();
//        task.findTheSmallestNumber();
//        task.computeTheAverage();
        task.findTheMiddleCharacter();
        task.countAllVowels();
    }

    public void findTheSmallestNumber() {
        System.out.println("Enter 3 numbers");
        int[] input = new int[3];
        for (int i = 0; i < input.length; i++) {
            input[i] = ScannerUtils.getNumberFromInput();
        }
        Arrays.sort(input);
        int res = input[0];
        System.out.println("The smallest number is " + res);
    }

    public void computeTheAverage() {
        System.out.println("Enter 3 numbers");
        int[] input = new int[3];
        for (int i = 0; i < input.length; i++) {
            input[i] = ScannerUtils.getNumberFromInput();
        }
        int sum = input[0] + input[1] + input[2];
        int res = sum / 3;

        System.out.println("The average of the 3 numbers is " + res);
    }

    public void findTheMiddleCharacter() {
        String text = "4567";
        System.out.println("The middle character of the string is " + middle(text));
    }

    public static String middle(String text) {
        int position;
        int length;
        if (text.length() % 2 == 0) {
            position = text.length() / 2 - 1;
            length = 2;
        } else {
            position = text.length() / 2;
            length = 1;
        }
        return text.substring(position, position + length);
    }

    public void countAllVowels(){
        String text = "Hello! My name is Ioana";
        int count = 0;

        for (int i = 0; i < text.length(); i++){
            if (text.charAt(i) == 'a' || text.charAt(i) == 'e' || text.charAt(i) == 'i' || text.charAt(i) == 'o' || text.charAt(i) == 'u'){
                count++;
            }
        }
        System.out.println("The number of vowels in the text is " + count);
    }

    // 5. Write a Java method to count all words in a string. Go to the editor
    //Test Data:
}
