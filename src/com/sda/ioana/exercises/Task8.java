package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Task8 task = new Task8();
        task.findLongestText();
    }

    public void findLongestText() {
        Scanner scanner = ScannerUtils.getScanner();
        System.out.println("Introduce how many texts you want, when u wanna stop, write 'Enough!'");
        String text;
        String longestText = "";
        do {
            text = scanner.nextLine();
            if (text.length() > longestText.length() && !text.equals("Enough!")) {
                longestText = text;
            }
        } while (!text.equals("Enough!"));
        if (longestText.length() < 1){
            System.out.println("No text provided");
        }
    }
}


