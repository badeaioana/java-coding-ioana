package com.sda.ioana.exercises.shopsimulator;

public class MainShop {
    public static void main(String[] args) {

        Simulator simulator = new Simulator();
        simulator.showProducts();
        simulator.chooseProducts();
        simulator.payProducts();


    }
}
