package com.sda.ioana.exercises.shopsimulator;

import com.sda.ioana.utils.ScannerUtils;

public class Simulator {

    private Product[] products;
    private Product[] basket;

    public Simulator() {
        products = new Product[5];
        products[0] = new Product("sampon", 10);
        products[1] = new Product("deodorant", 7);
        products[2] = new Product("gel de dus", 15);
        products[3] = new Product("crema hidratanta", 20);
        products[4] = new Product("bere", 15);

    }

    public void showProducts() {
        for (Product element : products) {
            System.out.println(element);
        }
    }

    public void chooseProducts() {
        basket = new Product[2];
        basket[0] = products[1];
        basket[1] = products[4];
    }

    public void payProducts() {
        double totalPrice = getTotalPrice();
        System.out.println("Your have to pay " + totalPrice);
        makeSale(totalPrice);
    }

    private void makeSale(double price) {
        double moneyFromUser = ScannerUtils.getFloatFromConsole();
        if (moneyFromUser > price) {
            double change = moneyFromUser - price;
            System.out.println("Thank you for shopping, here is your change: " + change);
        } else if (moneyFromUser < price) {
            double overPrice = price - moneyFromUser;
            System.out.println("You need to give me " + overPrice + " more");
            makeSale(overPrice);
        } else {
            System.out.println("Thank you for your shopping");
        }
    }

    private double getTotalPrice() {
        double total = 0;
        for (int i = 0; i < basket.length; i++) {
            total += basket[i].getPrice();
        }
        return total;
    }

}
