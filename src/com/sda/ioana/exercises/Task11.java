package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task11 {
    public static void main(String[] args) {
        Task11 task = new Task11();
        task.countLettersBetweenChar();
    }

    public void countLettersBetweenChar(){
        System.out.println("Introduce 2 letters from the alphabet");
        char firstLetter = ScannerUtils.getScanner().nextLine().charAt(0);
        char secondLetter = ScannerUtils.getScanner().nextLine().charAt(0);
        int numberOfLetters = secondLetter - firstLetter - 1;

        System.out.println("Between letter " + firstLetter + " and letter " + secondLetter + " are " + numberOfLetters + " letters");
    }
}
