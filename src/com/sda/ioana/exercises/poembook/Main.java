package com.sda.ioana.exercises.poembook;

public class Main {
    public static void main(String[] args) {

        Author mihaiEminescu = new Author("Eminescu", "Romanian");
        Author lucianBlaga = new Author("Blaga", "Romanian");
        Author bacovia = new Author("Bacovia", "Romanian");

        Poem plumb = new Poem(bacovia, 10);
        Poem ceTeLegeni = new Poem(mihaiEminescu, 14);
        Poem pamantul = new Poem(lucianBlaga, 12);

        Poem[] poems = {plumb, ceTeLegeni, pamantul};
        Main main = new Main();
        System.out.println("The author who wrote the longest poem is " + main.longestPoem(poems).getCreator().getSurname());


    }

    public Poem longestPoem(Poem[] poems) {
        Poem longestPoem = poems[0];
        for (Poem poem : poems) {
            if (longestPoem.getStropheNumbers() < poem.getStropheNumbers()) {
                longestPoem = poem;
            }
        }
        return longestPoem;
    }
}

