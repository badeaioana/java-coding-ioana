package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task2 {
    public static void main(String[] args) {
        Task2 task = new Task2();
        task.fizzBuzz();
    }

    public void fizzBuzz(){
        System.out.println("Introduceti un numar pozitiv: ");
        int number = ScannerUtils.getNumberFromInput();
        for (int i = 1; i <= number; i++) {
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("Fizz Buzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 7 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }
}
