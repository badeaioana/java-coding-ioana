package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task5 {

    public static void main(String[] args) {
        Task5 task = new Task5();
        task.TheSumOfTheHarmonicSeries();
    }

    public void TheSumOfTheHarmonicSeries() {
        System.out.println("Introduce a number");
        int n = ScannerUtils.getNumberFromInput();
        double sum = 0;

        for (float i = 1; i <= n; i++) {
            sum += (1 / i);
        }
        System.out.println(sum);
    }
}


