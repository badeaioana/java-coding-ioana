package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task1 {

    public static void main(String[] args) {
        System.out.println("Numarul introdus este: " + ScannerUtils.getNumberFromInput());
        calculateCirclePerimeter();
    }

    public static void calculateCirclePerimeter(){
        System.out.println("Introdu diametrul cercului: ");
        float diameter = ScannerUtils.getFloatFromConsole();
        System.out.println("Perimetrul cercului cu diametrul tau este: " + Math.PI * diameter);
    }

}
