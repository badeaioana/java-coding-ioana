package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

public class Task9 {
    public static void main(String[] args) {
        Task9 task = new Task9();
        task.findOccurrenceOfSpace();
    }

    public void findOccurrenceOfSpace() {
        System.out.println("Introduce a text");
        String text = ScannerUtils.getScanner().nextLine();
        int totalLength = text.length();
        int numberOfSpaces = text.split(" ").length - 1;
        int percentage = numberOfSpaces * 100 / totalLength;
        System.out.println("Procentul de spatii este: " + percentage + "%");

    }

}


