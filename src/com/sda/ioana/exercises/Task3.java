package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

import java.util.Scanner;

public class Task3 {

    public static void main(String[] args) {
        Task3 task = new Task3();
        task.calculatingBodyMassIndex();
    }

    public void calculatingBodyMassIndex() {
        System.out.println("Introduce your weight");
        float weight = ScannerUtils.getFloatFromConsole();
        System.out.println("Introduce your height");
        int height = ScannerUtils.getNumberFromInput();
        float bodyMassIndex = weight / height;

        if (bodyMassIndex < 18.5 || bodyMassIndex > 24.9) {
            System.out.println("BMI not optimal");
        } else {
            System.out.println("BMI optimal");
        }
    }
}
