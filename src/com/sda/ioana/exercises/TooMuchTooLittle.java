package com.sda.ioana.exercises;

import com.sda.ioana.utils.ScannerUtils;

import java.util.Random;

public class TooMuchTooLittle {
    public static void main(String[] args) {
        TooMuchTooLittle game = new TooMuchTooLittle();
        game.randomGame();
    }

    public void randomGame() {
        int myNumber = new Random().nextInt(100);
        System.out.println("Guess the number between 1 and 100");
        int number;
        int numberOfTries = 0;

        do {
            number = ScannerUtils.getNumberFromInput();
            if (number < myNumber) {
                System.out.println("Not enough");
                System.out.println("Enter another number");
                numberOfTries++;
            } else if (number > myNumber) {
                System.out.println("Too much");
                System.out.println("Enter another number");
                numberOfTries++;
            } else {
                numberOfTries++;
                System.out.println("Congratulations! You got it in: " + numberOfTries + " tries");
            }
        } while (number != myNumber);
    }
}
